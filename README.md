# Stage Git Standbox #

This is a place for you to play around with Git without messing up our main repo.
Here's a nice 5-minute tutorial:


1. Click `Fork` in the left-hand menu
2. Clone your forked repo (should be ```https://YOUR-NAME@bitbucket.org/YOUR-NAME/git-sandbox.git```)
3. Add upstream (the main ```stage-esports``` repo) with ```git remote add upstream https://YOUR-NAME@bitbucket.org/stage-esports/git-sandbox.git```
4. Make a change
5. Push your changes to ```origin```
6. Click ```Create pull request``` in BitBucket's left-hand menu (make sure you're in your **forked** repo, not upstream from step 3)
7. Tag someone to "review"
8. Click ```merge``` and submit the form
9. Go back to your terminal and ```git pull --rebase upstream master```
10. ```git log --pretty=format:'%h %ad | %s%d [%an]' --graph --date=short``` (I use this all the time and aliased it to ```git hist```)


![workflow-chart.png](./workflow-chart.png)